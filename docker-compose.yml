version: "3"
services:
  backend:
    image: registry.gitlab.com/dlr-shepard/backend:latest
    environment:
      CATALINA_OPTS: "-Xms2048m -Xmx2048m"
      oidc.public: ${OIDC_PUBLIC}
      oidc.authority: ${OIDC_AUTHORITY}
      influx.host: influxdb:8086
      influx.username: admin
      influx.password: ${INFLUX_PW}
      neo4j.host: neo4j:7687
      neo4j.username: neo4j
      neo4j.password: ${NEO4J_PW}
      mongo.host: mongodb:27017
      mongo.username: mongo
      mongo.password: ${MONGO_PW}
    ports:
      - "127.0.0.1:8080:8080"
    volumes:
      - /opt/shepard/backend/tomcat:/usr/local/tomcat/logs
      - /opt/shepard/backend/config:/root/.shepard
    restart: unless-stopped
    depends_on:
      - neo4j
      - mongodb
      - influxdb
    networks:
      - shepard

  frontend:
    image: registry.gitlab.com/dlr-shepard/frontend:latest
    environment:
      VUE_APP_BACKEND: ${BACKEND_URL}shepard/api
      VUE_APP_OIDC_AUTHORITY: ${OIDC_AUTHORITY}
      VUE_APP_CLIENT_ID: ${CLIENT_ID}
    ports:
      - "127.0.0.1:8081:80"
    restart: unless-stopped
    depends_on:
      - backend
    networks:
      - frontend

  swagger:
    image: swaggerapi/swagger-ui:latest
    environment:
      - URL=${BACKEND_URL}shepard/doc/openapi.json
    ports:
      - "127.0.0.1:8082:8080"
    restart: unless-stopped
    depends_on:
      - backend
    networks:
      - frontend

  neo4j:
    image: neo4j:4.4
    environment:
      NEO4J_AUTH: neo4j/${NEO4J_PW}
      NEO4J_dbms_memory_heap_initial__size: 2048M
      NEO4J_dbms_memory_heap_max__size: 2048M
      NEO4J_dbms_memory_pagecache_size: 3072M
    ports:
      - "127.0.0.1:7474:7474"
      - "7687:7687"
    volumes:
      - /opt/shepard/neo4j/logs:/logs
      - /opt/shepard/neo4j/plugins:/plugins
      - /opt/shepard/neo4j/data:/var/lib/neo4j/data
    restart: unless-stopped
    networks:
      - shepard

  mongodb:
    image: mongo:4.4
    environment:
      MONGO_INITDB_ROOT_USERNAME: mongo
      MONGO_INITDB_ROOT_PASSWORD: ${MONGO_PW}
      MONGO_INITDB_DATABASE: database
    volumes:
      - /opt/shepard/mongodb/db:/data/db
      - /opt/shepard/mongodb/configdb:/data/configdb
    restart: unless-stopped
    networks:
      - shepard
      - mongo
    command: --wiredTigerCacheSizeGB 2.0

  mongoexpress:
    image: mongo-express:latest
    environment:
      ME_CONFIG_MONGODB_SERVER: mongodb
      ME_CONFIG_BASICAUTH_USERNAME: mongo
      ME_CONFIG_BASICAUTH_PASSWORD: ${MONGO_PW}
      ME_CONFIG_MONGODB_ENABLE_ADMIN: "true"
      ME_CONFIG_MONGODB_ADMINUSERNAME: mongo
      ME_CONFIG_MONGODB_ADMINPASSWORD: ${MONGO_PW}
    ports:
      - "127.0.0.1:8084:8081"
    restart: unless-stopped
    depends_on:
      - mongodb
    networks:
      - mongo

  influxdb:
    image: influxdb:1.8-alpine
    environment:
      IFLUXDB_ADMIN_ENABLED: "true"
      INFLUXDB_HTTP_AUTH_ENABLED: "true"
      INFLUXDB_ADMIN_USER: admin
      INFLUXDB_ADMIN_PASSWORD: ${INFLUX_PW}
      INFLUXDB_LOGGING_LEVEL: warn
      INFLUXDB_HTTP_LOG_ENABLED: "false"
      INFLUXDB_DATA_CACHE_MAX_MEMORY_SIZE: 2048M
    volumes:
      - /opt/shepard/influxdb:/var/lib/influxdb
    restart: unless-stopped
    networks:
      - shepard
      - influxdata

  chronograf:
    image: chronograf:alpine
    environment:
      INFLUXDB_URL: http://influxdb:8086
      INFLUXDB_USERNAME: admin
      INFLUXDB_PASSWORD: ${INFLUX_PW}
    ports:
      - "127.0.0.1:8888:8888"
    volumes:
      - /opt/shepard/chronograf:/var/lib/chronograf
    restart: unless-stopped
    depends_on:
      - influxdb
    networks:
      - influxdata

networks:
  shepard:
  mongo:
  influxdata:
  frontend:
